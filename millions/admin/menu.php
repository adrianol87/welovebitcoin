<?php
/**
 * @version		$Id: menu.php 86 2010-10-12 13:51:14Z ryan $
 * @package		mds
 * @copyright	(C) Copyright 2010 Ryan Rhode, All rights reserved.
 * @author		Ryan Rhode, ryan@milliondollarscript.com
 * @license		This program is free software; you can redistribute it and/or modify
 *		it under the terms of the GNU General Public License as published by
 *		the Free Software Foundation; either version 3 of the License, or
 *		(at your option) any later version.
 *
 *		This program is distributed in the hope that it will be useful,
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *		GNU General Public License for more details.
 *
 *		You should have received a copy of the GNU General Public License along
 *		with this program;  If not, see http://www.gnu.org/licenses/gpl-3.0.html.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *		Million Dollar Script
 *		A pixel script for selling pixels on your website.
 *
 *		For instructions see README.txt
 *
 *		Visit our website for FAQs, documentation, a list team members,
 *		to post any bugs or feature requests, and a community forum:
 * 		http://www.milliondollarscript.com/
 *
 */

require("../config.php");
require ('admin_common.php');


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<style>
a {
	color:#000000;
	text-decoration: none;
		 
}

a:hover {
	color:#3399FF;
}
</style>
<TITLE> Menu </TITLE>
<link rel="stylesheet" type="text/css" href="../semantic/semantic.min.css">
<META NAME="Generator" CONTENT="EditPlus">
<META NAME="Author" CONTENT="">
<META NAME="Keywords" CONTENT="">
<META NAME="Description" CONTENT="">
<script type="text/javascript" src="../semantic/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="../semantic/semantic.min.js"></script>
</HEAD>

<BODY style=" font-family: 'Arial', sans-serif; font-size:10pt; color:#000000;  " bgcolor="#F4F4F4">

<div class="ui small menu">
	<!-- <div class="right menu"> -->
	<a class="item" href="main.php" target="main">Main Summary</a>
	<div class="menu">
		<div class="ui dropdown item">Pixel Inventory<i class="dropdown icon"></i>
			<div class="menu">
				<a class="item" href="inventory.php" target="main">Manage Grids</a>
				<a class="item" href="packs.php" target="main">Packages</a>
				<a class="item" href="price.php" target="main">Price Zones</a>
				<a class="item" href="nfs.php" target="main">Not For Sale</a>
				<a class="item" href="blending.php" target="main">Backgrounds</a>
				<a class="item" href="gethtml.php" target="main">Get HTML Code</a>
			</div>
		</div>
		<div class="ui dropdown item">Advertiser Admin<i class="dropdown icon"></i>
			<div class="menu">
				<a class="item" href="customers.php" target="main">List Advertisers</a>
				<div class="ui dropdown item">Current orders<i class="dropdown icon"></i>
					<div class="menu">
						<a class="item" href="orders.php?show=WA" target="main">Orders: Waiting</a>
						<a class="item" href="orders.php?show=CO" target="main">Orders: Completed</a>
					</div>
				</div>
				<div class="ui dropdown item">Non-current orders<i class="dropdown icon"></i>
					<div class="menu">
						<a class="item" href="orders.php?show=EX" target="main">Orders: Expired</a>
						<a class="item" href="orders.php?show=CA" target="main">Orders: Cancelled</a>
						<a class="item" href="orders.php?show=DE" target="main">Orders: Deleted</a>
					</div>
				</div>
				<a class="item" href="ordersmap.php" target="main">Map of Orders</a>
				<a class="item" href="transactions.php" target="main">Transaction Log</a>
			</div>
		</div>
		<div class="ui dropdown item">Pixel Admin<i class="dropdown icon"></i>
			<div class="menu">
				<a class="item" href="approve.php?app=N" target="main">Approve Pixels</a>
				<a class="item" href="approve.php?app=Y" target="main">Disapprove Pixels</a>
				<a class="item" href="process.php" target="main">Process Pixels</a>
			</div>
		</div>
		<div class="ui dropdown item">Report<i class="dropdown icon"></i>
			<div class="menu">
				<a class="item" href="ads.php" target="main">Ad List</a>
				<a class="item" href="list.php" target="main">Top Advertisers</a>
				<a class="item" href="email_queue.php" target="main">Outgoing Email</a>
			</div>
		</div>
		<div class="ui dropdown item">Clicks<i class="dropdown icon"></i>
			<div class="menu">
				<a class="item" href="top.php" target="main">Top Clicks</a>
				<a class="item" href="clicks.php" target="main">Click Reports</a>
			</div>
		</div>
		<div class="ui dropdown item">Configuration<i class="dropdown icon"></i>
			<div class="menu">
				<a class="item" href="edit_config.php" target="main">Main Config</a>
				<a class="item" href="language.php" target="main">Language</a>
				<a class="item" href="currency.php" target="main">Currencies</a>
				<a class="item" href="payment.php" target="main">Payment Modules</a>
				<a class="item" href="adform.php" target="main">Ad Form</a>
			</div>
		</div>
		<div class="ui dropdown item">Info<i class="dropdown icon"></i>
			<div class="menu">
				<a class="item" href="info.php" target="main">System Info</a>
				<a class="item" href="http://www.milliondollarscript.com" target="main">Script Home</a>
			</div>
		</div>
	</div>
	<div class="menu right">
		<div class="item">
			<a class="ui primary button" href="logout.php" target="main">Logout</a>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('.ui.dropdown').dropdown();
</script>

</BODY>
</HTML>
