<?php
/**
 * @version		$Id: orders.php 72 2010-09-12 01:31:46Z ryan $
 * @package		mds
 * @copyright	(C) Copyright 2010 Ryan Rhode, All rights reserved.
 * @author		Ryan Rhode, ryan@milliondollarscript.com
 * @license		This program is free software; you can redistribute it and/or modify
 *		it under the terms of the GNU General Public License as published by
 *		the Free Software Foundation; either version 3 of the License, or
 *		(at your option) any later version.
 *
 *		This program is distributed in the hope that it will be useful,
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *		GNU General Public License for more details.
 *
 *		You should have received a copy of the GNU General Public License along
 *		with this program;  If not, see http://www.gnu.org/licenses/gpl-3.0.html.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *		Million Dollar Script
 *		A pixel script for selling pixels on your website.
 *
 *		For instructions see README.txt
 *
 *		Visit our website for FAQs, documentation, a list team members,
 *		to post any bugs or feature requests, and a community forum:
 * 		http://www.milliondollarscript.com/
 *
 */

session_start();
include ("../config.php");

include ("login_functions.php");

process_login();


require ("header.php");


?>

<script language="JavaScript" type="text/javascript">

function confirmLink(theLink, theConfirmMsg)
   {
      
       if (theConfirmMsg == '' || typeof(window.opera) != 'undefined') {
           return true;
       }

       var is_confirmed = confirm(theConfirmMsg + '\n');
       if (is_confirmed) {
           theLink.href += '&is_js_confirmed=1';
       }

       return is_confirmed;
   } // end of the 'confirmLink()' function

</script>
<section class="iContent">
<h4 class="ui horizontal divider header">
	<?php echo $label['advertiser_ord_history']; ?>
</h4>

<?php echo $label['advertiser_ord_explain']; ?>

<h4 class="ui horizontal divider header">
	<?php echo $label['advertiser_ord_hist_list']; ?>
</h4>

<?php
$sql = "SELECT * FROM orders as t1, users as t2 where t1.user_id=t2.ID AND t1.user_id='".intval($_SESSION['MDS_ID'])."' ORDER BY t1.order_date DESC ";
$result = mysqli_query($GLOBALS['connection'], $sql) or die (mysqli_error($GLOBALS['connection']));


?>

<table class="ui celled table" style="width: 90% !important; margin: 10px auto 20px;">
  <thead>
  <tr>
    <th><?php echo $label['advertiser_ord_prderdate']; ?></th>
    <th><?php echo $label['advertiser_ord_custname']; ?></th>
    <th><?php echo $label['advertiser_ord_usernid'];?></th>
	<th><?php echo $label['advertiser_ord_orderid']; ?></th>
	<th><?php echo $label['advertiser_ord_quantity']; ?></th>
	<th><?php echo $label['advertiser_ord_image']; ?></th>
	<th><?php echo $label['advertiser_ord_amount']; ?></th>
	<th><?php echo $label['advertiser_status']; ?></th>
	<th>Share</th>
   </tr>
</thead>
<?php

if (mysqli_num_rows($result)==0) {
	echo '<td colspan="7">'.$label['advertiser_ord_noordfound'].' </td>';
} else {

	while ($row=mysqli_fetch_array($result)) {
	?>
<tr onmouseover="old_bg=this.getAttribute('bgcolor');this.setAttribute('bgcolor', '#FBFDDB', 0);" onmouseout="this.setAttribute('bgcolor', old_bg, 0);" bgColor="#ffffff">
    <td><font face="Arial" size="2"><?php echo get_local_time($row['order_date']);?></font></td>
	<td><font face="Arial" size="2"><?php echo $row['FirstName']." ".$row['LastName'];?></font></td>
    <td><font face="Arial" size="2"><?php echo $row['Username'];?> (#<?php echo $row['ID'];?>)</font></td>
	<td><font face="Arial" size="2">#<?php echo $row['order_id'];?></font></td>
	<td><font face="Arial" size="2"><?php echo $row['quantity'];?></font></td>
	<td><font face="Arial" size="2"><?php 

			$sql = "select * from banners where banner_id=".intval($row['banner_id']);
			$b_result = mysqli_query($GLOBALS['connection'], $sql) or die (mysqli_error($GLOBALS['connection']).$sql);
			$b_row = mysqli_fetch_array($b_result);
		
			echo $b_row['name'];
			
		?></font></td>
	<td><font face="Arial" size="2"><?php echo convert_to_default_currency_formatted($row['currency'], $row['price']); ?></font></td>
	<td><font face="Arial" size="2"><?php echo $label[$row['status']];?><br><?php
	if (USE_AJAX=='SIMPLE') {
		$order_page = 'order_pixels.php';
		$temp_var = '&order_id=temp';
	} else {
		$order_page = 'select.php';
	}
	switch ($row['status']) {
		case "new":
			echo $label['adv_ord_inprogress'].'<br>';
			echo "<a href='".$order_page."?BID=".$row['banner_id']."$temp_var'>(".$label['advertiser_ord_confnow'].")</a>";
			echo "<br><input type='button' value='".$label['advertiser_ord_cancel_button']."' onclick='if (!confirmLink(this, \"".$label['advertiser_ord_cancel']."\")) return false; window.location=\"orders.php?cancel=yes&order_id=".$row['order_id']."\"' >";
			break;
		case "confirmed":
			echo "<a href='payment.php?order_id=".$row['order_id']."&BID=".$row['banner_id']."'>(".$label['advertiser_ord_awaiting'].")</a>";
			//echo "<br><input type='button' value='".$label['advertiser_ord_cancel_button']."' onclick='if (!confirmLink(this, \"".$label['advertiser_ord_cancel']."\")) return false; window.location=\"orders.php?cancel=yes&order_id=".$row['order_id']."\"' >";
			break;
		case "completed":
			echo "<a href='publish.php?order_id=".$row['order_id']."&BID=".$row['banner_id']."'>(".$label['advertiser_ord_manage_pix'].")</a>";

			if ($row['days_expire'] > 0) {

				if ($row['published']!='Y') {
						$time_start = strtotime(gmdate('r'));
				} else {
					$time_start = strtotime($row['date_published']." GMT");
				}

				$elapsed_time = strtotime(gmdate('r')) - $time_start;
				$elapsed_days = floor ($elapsed_time / 60 / 60 / 24);
				
				$exp_time =  ($row['days_expire']  * 24 * 60 * 60);

				$exp_time_to_go = $exp_time - $elapsed_time;
				$exp_days_to_go =  floor ($exp_time_to_go / 60 / 60 / 24);

				$to_go = elapsedtime($exp_time_to_go);

				$elapsed = elapsedtime($elapsed_time);

				if ($row['date_published']!='') {
					echo "<br>Expires in: ".$to_go;
				}

			}

			break;
		case "expired":

			$time_expired = strtotime($row['date_stamp']);

			$time_when_cancel = $time_expired + (DAYS_RENEW * 24 * 60 * 60);

			$days =floor (($time_when_cancel - time()) / 60 / 60 / 24);

			// check to see if there is a renew_wait or renew_paid order

			$sql = "select order_id from orders where (status = 'renew_paid' OR status = 'renew_wait') AND original_order_id='".intval($row['original_order_id'])."' ";
			$res_c = mysqli_query($GLOBALS['connection'], $sql);
			if (mysqli_num_rows($res_c)==0) {
 
				$label['advertiser_ord_renew'] = str_replace("%DAYS_TO_RENEW%", $days, $label['advertiser_ord_renew']);
				echo "<a href='payment.php?order_id=".$row['order_id']."&BID=".$row['banner_id']."'><font color='red' size='1'>(".$label['advertiser_ord_renew'].")</font></a>";
			}
			break;
		case "cancelled":
			break;
		case "pending":
			break;

	}

/*
	if (($row['price']==0) && ($row['status']='deleted') && && ($row['status']!='cancelled')) {

		echo "<br><input type='button' value='".$label['advertiser_ord_cancel_button']."' onclick='if (!confirmLink(this, \"".$label['advertiser_ord_cancel']."\")) return false; window.location=\"orders.php?cancel=yes&order_id=".$row['order_id']."\"' >";


	}

*/
	
	?></td>
	<td>
      <div class="share-nav">
        <a href="https://www.facebook.com/sharer/sharer.php?u=www.cryptopxl.com/" target="_blank" alt="Share Facebook" class="st-btn st-first st-remove-label" data-network="facebook">
          <svg fill="#3B5998" preserveAspectRatio="xMidYMid meet" height="1.5em" width="1.5em" viewBox="0 0 40 40" style="border: 1px #3B5998 solid;border-radius: 5px;">
            <g>
              <path d="m21.7 16.7h5v5h-5v11.6h-5v-11.6h-5v-5h5v-2.1c0-2 0.6-4.5 1.8-5.9 1.3-1.3 2.8-2 4.7-2h3.5v5h-3.5c-0.9 0-1.5 0.6-1.5 1.5v3.5z"></path>
            </g>
          </svg>
        </a>
        <a href="https://twitter.com/home?status=www.cryptopxl.com/" target="_blank" alt="Share Twitter" class="st-btn st-remove-label" data-network="twitter" style="display: inline-block;">
          <svg fill="#55acee" preserveAspectRatio="xMidYMid meet" height="1.5em" width="1.5em" viewBox="0 0 40 40" style="border: 1px #55acee solid;border-radius: 5px;">
            <g>
              <path d="m31.5 11.7c1.3-0.8 2.2-2 2.7-3.4-1.4 0.7-2.7 1.2-4 1.4-1.1-1.2-2.6-1.9-4.4-1.9-1.7 0-3.2 0.6-4.4 1.8-1.2 1.2-1.8 2.7-1.8 4.4 0 0.5 0.1 0.9 0.2 1.3-5.1-0.1-9.4-2.3-12.7-6.4-0.6 1-0.9 2.1-0.9 3.1 0 2.2 1 3.9 2.8 5.2-1.1-0.1-2-0.4-2.8-0.8 0 1.5 0.5 2.8 1.4 4 0.9 1.1 2.1 1.8 3.5 2.1-0.5 0.1-1 0.2-1.6 0.2-0.5 0-0.9 0-1.1-0.1 0.4 1.2 1.1 2.3 2.1 3 1.1 0.8 2.3 1.2 3.6 1.3-2.2 1.7-4.7 2.6-7.6 2.6-0.7 0-1.2 0-1.5-0.1 2.8 1.9 6 2.8 9.5 2.8 3.5 0 6.7-0.9 9.4-2.7 2.8-1.8 4.8-4.1 6.1-6.7 1.3-2.6 1.9-5.3 1.9-8.1v-0.8c1.3-0.9 2.3-2 3.1-3.2-1.1 0.5-2.3 0.8-3.5 1z"></path>
            </g>
          </svg>
        </a>
        <a href="https://www.linkedin.com/shareArticle?mini=true&url=www.cryptopxl.com/" target="_blank" alt="Share Linkedin" class="st-btn st-last st-remove-label" data-network="linkedin" style="display: inline-block;">
          <svg fill="#0077b5" preserveAspectRatio="xMidYMid meet" height="1.5em" width="1.5em" viewBox="0 0 40 40" style="border: 1px #0077b5 solid;border-radius: 5px;">
            <g>
              <path d="m13.3 31.7h-5v-16.7h5v16.7z m18.4 0h-5v-8.9c0-2.4-0.9-3.5-2.5-3.5-1.3 0-2.1 0.6-2.5 1.9v10.5h-5s0-15 0-16.7h3.9l0.3 3.3h0.1c1-1.6 2.7-2.8 4.9-2.8 1.7 0 3.1 0.5 4.2 1.7 1 1.2 1.6 2.8 1.6 5.1v9.4z m-18.3-20.9c0 1.4-1.1 2.5-2.6 2.5s-2.5-1.1-2.5-2.5 1.1-2.5 2.5-2.5 2.6 1.2 2.6 2.5z"></path>
            </g>
          </svg>
        </a>
      </div>
	</td>
	</tr>

	<?php
	}


}
?>

</table>

</section>
<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5c3391336aa2aa0011452015&product='inline-share-buttons' async='async'></script>

<?php

require ("footer.php");

?>