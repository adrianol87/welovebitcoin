<?php
error_reporting(0);

#########################################################################
# CONFIGURATION
# Note: Please do not edit this file. Edit the config from the admin section.
#########################################################################

define('DEBUG', false);
define('MDS_LOG', false);
define('MDS_LOG_FILE', dirname(__FILE__).'/.mds.log');

define('VERSION_INFO', 'v 2.1 (Oct 2010)');

define('BASE_HTTP_PATH', 'https://www.cryptopxl.com/millions/'); 
define('BASE_PATH', '/home/a9rdrnmadxcp/public_html/millions');
define('SERVER_PATH_TO_ADMIN', '/home/a9rdrnmadxcp/public_html/millions/admin/');
define('UPLOAD_PATH', '/home/a9rdrnmadxcp/public_html/millions/upload_files/');
define('UPLOAD_HTTP_PATH', 'https://www.cryptopxl.com/millions/upload_files/');
define('MYSQL_HOST', 'localhost'); # mysql database host
define('MYSQL_USER', 'a9rdrnmadxcp'); #mysql user name
define('MYSQL_PASS', 'iGwV9snFI=:'); # mysql password
define('MYSQL_DB', 'i5118441_wp1'); # mysql database name
define('MYSQL_PORT', 3306); # mysql port
define('MYSQL_SOCKET', ''); # mysql socket

# ADMIN_PASSWORD
define('ADMIN_PASSWORD',  'ok');

define('MDS_RESIZE', 'YES');

# SITE_CONTACT_EMAIL
define('SITE_CONTACT_EMAIL', stripslashes('information@cryptopxl.com'));

# SITE_LOGO_URL
define('SITE_LOGO_URL', stripslashes('https://www.cryptopxl.com/wp-content/uploads/2018/11/logo-dark-01-1.png'));

# SITE_NAME
# change to your website name
define('SITE_NAME', stripslashes('CryptoPXL')); 

# SITE_SLOGAN
# change to your website slogan
define('SITE_SLOGAN', stripslashes('Join to the WE LOVE CRYPTO WALL and be part of the crypto history.')); 

# date formats
define('DATE_FORMAT', 'Y-M-d');
define('GMT_DIF', '6');
define('DATE_INPUT_SEQ', 'YMD');

# Output the image in JPEG? Y or N. 
define ('OUTPUT_JPEG', 'Y'); # Y or N
define ('JPEG_QUALITY', '60'); # a number from 0 to 100
define('INTERLACE_SWITCH','NO');

# Note: Please do not edit this file. Edit from the admin section.

# USE_LOCK_TABLES
# The script can lock/unlock tables when a user is selecting pixels
define ('USE_LOCK_TABLES', 'Y');

define('BANNER_DIR', 'pixels/');

# IM_CONVERT_PATH
define('IM_CONVERT_PATH', 'IM_CONVERT_PATH');

# Note: Please do not edit this file. Edit from the admin section.

define('EMAIL_USER_ORDER_CONFIRMED', 'YES');
define('EMAIL_ADMIN_ORDER_CONFIRMED', 'YES');
define('EMAIL_USER_ORDER_COMPLETED', 'YES');
define('EMAIL_ADMIN_ORDER_COMPLETED', 'YES');
define('EMAIL_USER_ORDER_PENDED', 'YES');
define('EMAIL_ADMIN_ORDER_PENDED', 'YES');
define('EMAIL_USER_ORDER_EXPIRED', 'YES');
define('EMAIL_ADMIN_ORDER_EXPIRED', 'YES');

define('EM_NEEDS_ACTIVATION', 'YES');
define('EMAIL_ADMIN_ACTIVATION', 'YES');
define('EMAIL_ADMIN_PUBLISH_NOTIFY', 'YES');
define('USE_PAYPAL_SUBSCR', 'USE_PAYPAL_SUBSCR');
define('EMAIL_USER_EXPIRE_WARNING', '');
define('DAYS_RENEW', '7');
define('DAYS_CONFIRMED', '7');
define('HOURS_UNCONFIRMED', '1');
define('DAYS_CANCEL', '3');
define('ENABLE_MOUSEOVER', 'YES');
define('ENABLE_CLOAKING', 'YES');
define('VALIDATE_LINK', 'NO');
define('DISPLAY_PIXEL_BACKGROUND', 'NO');
define('USE_SMTP', '');
define('EMAIL_SMTP_SERVER', '');
define('EMAIL_SMTP_USER', '');
define('EMAIL_SMTP_PASS', '');
define('EMAIL_SMTP_AUTH_HOST', '');
define('SMTP_PORT', '465');
define('POP3_PORT', '995');
define('EMAIL_TLS', '1');
define('EMAIL_POP_SERVER', '');
define('EMAIL_POP_BEFORE_SMTP', '');
define('EMAIL_DEBUG', 'NO');

define('EMAILS_PER_BATCH', '12');
define('EMAILS_MAX_RETRY', '15');
define('EMAILS_ERROR_WAIT', '20');
define('EMAILS_DAYS_KEEP', '30');
define('USE_AJAX', 'NO');
define('ANIMATION_SPEED', '50');
define('MAX_BLOCKS', '');
define('MEMORY_LIMIT', '256M');

define('REDIRECT_SWITCH', 'NO');
define('REDIRECT_URL', 'https://www.example.com');
define('ADVANCED_CLICK_COUNT', 'YES');

define('HIDE_TIMEOUT', '500');
define('MDS_AGRESSIVE_CACHE', '');

if (defined('MEMORY_LIMIT')) {
	ini_set('memory_limit', MEMORY_LIMIT);
} else {
	ini_set('memory_limit', '64M');
}

define('ERROR_REPORTING', 0);

	// database connection
	require_once(dirname(__FILE__).'/include/database.php');

	// load HTMLPurifier
    require_once dirname(__FILE__).'/vendor/ezyang/htmlpurifier/library/HTMLPurifier.auto.php';
    $purifier = new HTMLPurifier(); 
	
	require_once dirname(__FILE__).'/include/functions2.php';
	$f2 = new functions2();

	include dirname(__FILE__).'/lang/lang.php';
	require_once dirname(__FILE__).'/vendor/phpmailer/phpmailer/PHPMailerAutoload.php';
	require_once dirname(__FILE__).'/include/mail_manager.php';
	require_once dirname(__FILE__).'/include/currency_functions.php';
	require_once dirname(__FILE__).'/include/price_functions.php';
	require_once dirname(__FILE__).'/include/functions.php';
	require_once dirname(__FILE__).'/include/image_functions.php';
	if (!get_magic_quotes_gpc()) unfck_gpc();
	//escape_gpc();

function get_banner_dir() {
	if ( BANNER_DIR == 'BANNER_DIR' ) {

		$base = BASE_PATH;
		if ( $base == 'BASE_PATH' ) {
			$base = __DIR__;
		}
		$dest = $base . '/banners/';

		if ( file_exists( $dest ) ) {
			$BANNER_DIR = 'banners/';
		} else {
			$BANNER_DIR = 'pixels/';
		}
	} else {
		$BANNER_DIR = BANNER_DIR;
	}

	return $BANNER_DIR;

}

?>